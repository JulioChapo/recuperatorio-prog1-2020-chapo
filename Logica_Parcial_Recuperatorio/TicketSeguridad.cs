﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Parcial_Recuperatorio
{
    public class TicketSeguridad
    {
        public double CodigoAutonumerico { get; set; }
        //CORRECCIÓN: ESTO DEBERÍA SER UN ENUMERADOR
        public int TipoTicket { get; set; }
        public DateTime FechaCreacion { get; set; }
        public Empleado Datos { get; set; }
        //CORRECCIÓN: ESTO DEBERÍA SER UN ENUMERADOR
        public int EstadoTicket { get; set; }
        public string ObservacionUsuario { get; set; }
        public string ComentarioResolucion { get; set; }


        //CORRRECCIÓN: INCORRECTO, ESTO NO SE HACE DE ESTA MANERA, ADEMAS ESTABA COMO DATO ANEXO, NO DEBÍA RESOLVERSE
        int acumulador = 0;
        public void GenerarCodigoAutoNumerico()
        {
            acumulador++;
            CodigoAutonumerico = acumulador;
        }
    }
}
