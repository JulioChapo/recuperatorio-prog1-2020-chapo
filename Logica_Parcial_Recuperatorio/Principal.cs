﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Parcial_Recuperatorio
{
    public class Principal
    {
        List<Empleado> ListaEmpleados = new List<Empleado>();
        List<TicketSeguridad> Tickets = new List<TicketSeguridad>();
       
        //CORRECCIÓN: ESTO SE RESUELVE CON UN MÉTODO ABSTRACTO EN EMPLEADO Y LUEGO EN CADA TIPO DE EMPLEADO SE SOBREESCRIBE Y RESUELVEN LAS CONDICIONES PARTICULARES DE CADA UNO.
        public bool Validacion(int tipoTicket,int Dni )
        {
            foreach (var item in ListaEmpleados)
            {
                if (Dni == item.DNI)
                {
                    if (item is Desarrollador)
                    {
                        Desarrollador aux = item as Desarrollador;
                        if (aux.Jefe.AreaACargo == 1 && tipoTicket == 2 )
                        {
                            return true;
                        }
                        return false;
                    }
                    else if (tipoTicket == 1 || tipoTicket == 3)                        
                    {
                        return true;
                    }
                    else
                        return false;                       
                }
                return false;
            }
            return false;
        }

        public void ActualizarEstado (double numeroTicket,int estado, string comentario)
        {
            foreach (var item in Tickets)
            {
                //CORRECCIÓN: FALTAN VALIDACIONES DEL SENTIDO, SI EL TICKET ESTÁ "EN PROCESO" NO PUEDE PONERSE EN "NUEVO"
                if (item.CodigoAutonumerico == numeroTicket)
                {
                    item.EstadoTicket = estado;
                    item.ComentarioResolucion = comentario;
                }
            }
        }
    }
}
