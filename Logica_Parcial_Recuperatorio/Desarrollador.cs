﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Parcial_Recuperatorio
{
    public class Desarrollador: Empleado
    {
        public string NivelExperiencia { get; set; }
        public Lider Jefe { get; set; }

        //CORRECCIÓN: PARA DEVOLVER EL NOMBRE DEBES HACER JEFE.NOMBRE
        public override string Buscardetalles()
        {
            return $"{base.Buscardetalles()}, y su lider de proyecto es: {Jefe}.";
        }
    }
}
